require('dotenv').config()
const http = require('http');
const { createEventAdapter } = require('@slack/events-api');
const slackEvents = createEventAdapter(process.env.SLACK_SIGNING_SECRET);
const port = process.env.PORT || 3000;
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const {handeSlackEvents}=require('./buisness_modules/slack');
 



// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*")
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
//   next()
// })


app.use('/slack/events', slackEvents.expressMiddleware());
slackEvents.on('message',handeSlackEvents);
slackEvents.on('error', console.error);
 
 

 module.exports=app









// Start the express application
http.createServer(app).listen(port, () => {
  console.log(`server listening on port ${port}`);
});