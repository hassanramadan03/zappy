require('dotenv').config()
// const {room}=require('../../index')
var Twit = require('twit')

const FB = require('fb');
FB.setAccessToken(process.env.FB_ACCESS_TOKEN);

const handleFBPosts = async () => {
    try {
          let {data}=await  FB.api('me/feed', 'get')
          console.log('fb posts ',data.length);
          
        // room.sentPosts(allPosts)
    } catch (error) {
        console.log(error);
    }
}
const handleTWPosts=async()=>{
    var T = new Twit({
      consumer_key:process.env.consumer_key,
      consumer_secret:process.env.consumer_secret,
      access_token:   process.env.access_token,
      access_token_secret: process.env.access_token_secret,
      timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
      strictSSL:            true,     // optional - requires SSL certificates to be valid.
    })
     
    //
    //  id: 1105408238869205000,
    //  id_str: '1105408238869204992',
    //id: 1106278054467457000,
  //id_str: '1106278054467457026'
    T.get('statuses/user_timeline', { count: 200 }, function(err, data, response) {
      console.log('TW posts',data.length)
    })
}
module.exports = {
    handleFBPosts,
    handleTWPosts
}