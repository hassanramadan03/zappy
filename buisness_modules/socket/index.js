class Room {
    constructor(io) {
        this.namespace = io;
        this.namespace.on('connection',this.handleConnection);
        this.namespace.on('disconnect', this.handledisConnection);
    }

    handleConnection() {
        console.log('connected');
    }

    handledisConnection() {
        console.log('disconnected');
    }

    sendPosts(allPosts){
        console.log('from socket',allPosts);
        this.namespace.emit({allPosts})
    }
}




module.exports = {
    Room 
};